package tr.org.ito.app.activities.announcement;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import tr.org.ito.app.R;
import tr.org.ito.app.activities.BaseActivity;
import tr.org.ito.app.activities.onlineoperations.OnlineOperationItem;
import tr.org.ito.app.helpers.APIManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Q on 02.02.2016.
 TODO:
 -  son giristen sonra yeni duyurularin yanina ünlem konacak veya başına height kadar bir şerit kırmızı renkte.
 -  menüde de yeni duyuru varsa ünlem konacak. servera gidip gelme overheadi var ama mantıklı feature.
 -  önemli duyurunun rengi farklı olabilir.
 -  duyuruların başına duyuru iconu konabilir
 -  gruplama ay ay yapılabilir. tabsız duyurunun başına section olarak ay yazılır. 2. sayfaya geçtiğinde yine aynı şekilde
 -  duyuruların tarihe göre sıralı geldiği varsayılıyor.
 */
public class AnnouncementsActivity extends BaseActivity {
    AnnouncementFragmentAdapter mAdapter;
    ViewPager mPager;
    private TabLayout mTabLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement);
        init();

        mAdapter = new AnnouncementFragmentAdapter(getSupportFragmentManager());

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mPager = (ViewPager) findViewById(R.id.viewpager);

        mPager.setAdapter(mAdapter);
        mTabLayout.setTabsFromPagerAdapter(mAdapter);

        mTabLayout.setupWithViewPager(mPager);
        mPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getIntent() != null &&
                        getIntent().getStringExtra("search") != null &&
                        !getIntent().getStringExtra("search").equals("")) {
                    onQueryTextChange(String.valueOf(getIntent().getStringExtra("search")));
                }
            }
        }, 150);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        AnnouncementFragment currentFragment =
                (AnnouncementFragment) mAdapter.instantiateItem(mPager, mPager.getCurrentItem());

        currentFragment.filter(newText);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }
}
