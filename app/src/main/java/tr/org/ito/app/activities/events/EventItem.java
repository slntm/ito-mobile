package tr.org.ito.app.activities.events;

import android.graphics.Bitmap;

/**
 * Created by Q on 07.02.2016.
 */
public class EventItem {
    public Bitmap image;
    public String headline;
    public String date;
    public String location;

    public EventItem(Bitmap image, String date, String headline, String location) {
        this.date = date;
        this.headline = headline;
        this.image = image;
        this.location = location;
    }
}
