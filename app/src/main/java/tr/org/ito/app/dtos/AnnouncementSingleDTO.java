package tr.org.ito.app.dtos;

/**
 * Created by Q on 06.02.2016.
 */
public class AnnouncementSingleDTO {
    public String title;
    public String month;
    public String year;
    public String date;
    public String text;
    public String attachment;

    public AnnouncementSingleDTO(String titleString, String date, String month, String year, String text, String attachment) {
        this.attachment = attachment;
        this.date = date;
        this.month = month;
        this.text = text;
        this.title = titleString;
        this.year = year;
    }
}
