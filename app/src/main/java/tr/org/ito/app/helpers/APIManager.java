package tr.org.ito.app.helpers;

import tr.org.ito.app.R;
import tr.org.ito.app.activities.headlines.MainActivity;
import tr.org.ito.app.activities.onlineoperations.items.DeptInquiryActivity;
import tr.org.ito.app.activities.onlineoperations.items.VisaInquiryActivity;
import tr.org.ito.app.dtos.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Q on 31.01.2016.
 */
public class APIManager {
    public static ArrayList<SearchResultItem> searchableItems = new ArrayList<>();

    public static LinkedHashMap<AnnouncementsDTO.AnnouncementDate, ArrayList<AnnouncementSingleDTO>> getAnnouncementsList() {
        ArrayList<AnnouncementSingleDTO> list = new ArrayList<>();
        list.add(new AnnouncementSingleDTO("İDTM - İkili Görüşmeler ", "05.02.2016", "Şubat", "2016", "Text", "someFiles.pdf"));
        list.add(new AnnouncementSingleDTO("TİM – Tayvan Heyeti Mart 2016 hk. ", "05.02.2016", "Şubat", "2016", "Text", "someFiles.docx"));
        list.add(new AnnouncementSingleDTO("Karayolu Taşıma Yönetmeliği çerçevesinde düzenlenen sözleşmeler hk ", "05.02.2016", "Şubat", "2016", "Text", ""));
        list.add(new AnnouncementSingleDTO("İstanbul İhracatçı Birlikleri İran Sektörel Ticaret Heyeti Hk.", "04.02.2016", "Şubat", "2016", "Text", ""));
        list.add(new AnnouncementSingleDTO("Mısır İhracat Kayıt Sistemi ", "04.02.2016", "Şubat", "2016", "Text", ""));
        list.add(new AnnouncementSingleDTO("İstanbul İhracatçı Birlikleri İran Sektörel Ticaret Heyeti Hk.5", "04.02.2016", "Şubat", "2016", "Text", ""));
        list.add(new AnnouncementSingleDTO("Afrika Büyük Göller Bölgesi (ABGB) konferansı hk. ", "04.02.2016", "Şubat", "2016", "Text", ""));
        list.add(new AnnouncementSingleDTO("Vietnam Fuarları hk. ", "03.02.2016", "Şubat", "2016", "Text", ""));
        list.add(new AnnouncementSingleDTO("“Seveso Bildirim Sistemi” hk. ", "01.02.2016", "Şubat", "2016", "Text", ""));
        list.add(new AnnouncementSingleDTO("İhracatta Kullanılan Ahşap Ambalaj Malz. hk. ", "28.01.2016", "Ocak", "2016", "Text", ""));
        list.add(new AnnouncementSingleDTO("SIAL China Fuarı hk. ", "28.01.2016", "Ocak", "2016", "Text", ""));
        list.add(new AnnouncementSingleDTO("Karayolları, Köprüler ve Tüneller İhtisas Fuarı Hk. ", "28.01.2016", "Ocak", "2016", "Text", ""));
        list.add(new AnnouncementSingleDTO("Fuar duyurusu hk. ", "20.01.2016", "Ocak", "2016", "Text", ""));
        list.add(new AnnouncementSingleDTO("Yayımcılıkta Telif Hakları Sempozyumu Hk. ", "17.12.2015", "Aralık", "2015", "Text", ""));
        list.add(new AnnouncementSingleDTO("İTO BORÇ SORGULAMA / ÖDEME (YENİ) ", "02.04.2015", "Nisan", "2015", "Text", ""));
        list.add(new AnnouncementSingleDTO("Tehlikesiz Atık Üreticilerinin Beyan Zorunluluğu Hk. ", "16.12.2014", "Aralık", "2014", "Text", ""));
        return new AnnouncementsDTO(list).announcements;
    }

    public static ArrayList<OnlineOperationsDTO> getOnlineOperationsList() {
        ArrayList<OnlineOperationsDTO> list = new ArrayList<>();
        list.add(new OnlineOperationsDTO("Borc Durumu Sorgulama", "Kimlik ve ilgili tarih ile borc durumunuzu sorgulayın", DeptInquiryActivity.class, true));
        list.add(new OnlineOperationsDTO("Etkinliğe Kayıt Olma", "İTO altında düzenlenen etkinliklere kaydınızı yaptırın", MainActivity.class, true));
        list.add(new OnlineOperationsDTO("Vize Sorgulama", "", VisaInquiryActivity.class, false));
        list.add(new OnlineOperationsDTO("Tescil Başvuru Sonucu Sorgulama", "", MainActivity.class, false));
        return list;
    }

    public static ArrayList<HeadlinesDTO> getHeadlinesList() {
        //TODO: TEMP NASILSA ÇALIŞIYOR DİYE BURAYA KONDU.
        if (searchableItems.size() == 0) {
            searchableItems.add(new SearchResultItem("Çağlar: Türkiye, İran’ın en güclü partneri olacak", "Haberler"));
            searchableItems.add(new SearchResultItem("İstanbul tüccarı tek yürek", "Haberler"));
            searchableItems.add(new SearchResultItem("İstanbul Ticaret Odası’ndan duyuru", "Haberler"));
            searchableItems.add(new SearchResultItem("İTO'nun Yaşayan İstanbul maketi İstanbullularla buluştu", "Haberler"));
            searchableItems.add(new SearchResultItem("365 gün İstanbul turizmi ", "Haberler"));
            searchableItems.add(new SearchResultItem("En çok ‘Tax Free’ alışverişi komşu İran yaptı", "Haberler"));
            searchableItems.add(new SearchResultItem("Bu endeks ticaretin nabzını ölçüyor", "Haberler"));
            searchableItems.add(new SearchResultItem("İklim zararına risk transferi", "Haberler"));

            searchableItems.add(new SearchResultItem("İDTM - İkili Görüşmeler ", "Duyurular"));
            searchableItems.add(new SearchResultItem("TİM – Tayvan Heyeti Mart 2016 hk. ", "Duyurular"));
            searchableItems.add(new SearchResultItem("Karayolu Taşıma Yönetmeliği çerçevesinde düzenlenen sözleşmeler hk", "Duyurular"));
            searchableItems.add(new SearchResultItem("İstanbul İhracatçı Birlikleri İran Sektörel Ticaret Heyeti Hk.", "Duyurular"));
            searchableItems.add(new SearchResultItem("Mısır İhracat Kayıt Sistemi", "Duyurular"));
            searchableItems.add(new SearchResultItem("İstanbul İhracatçı Birlikleri İran Sektörel Ticaret Heyeti Hk.5", "Duyurular"));
            searchableItems.add(new SearchResultItem("Afrika Büyük Göller Bölgesi (ABGB) konferansı hk.", "Duyurular"));
            searchableItems.add(new SearchResultItem("Vietnam Fuarları hk.", "Duyurular"));
            searchableItems.add(new SearchResultItem("“Seveso Bildirim Sistemi” hk.", "Duyurular"));
            searchableItems.add(new SearchResultItem("İhracatta Kullanılan Ahşap Ambalaj Malz. hk.", "Duyurular"));
            searchableItems.add(new SearchResultItem("SIAL China Fuarı hk.", "Duyurular"));
            searchableItems.add(new SearchResultItem("Karayolları, Köprüler ve Tüneller İhtisas Fuarı Hk.", "Duyurular"));
            searchableItems.add(new SearchResultItem("Fuar duyurusu hk.", "Duyurular"));
            searchableItems.add(new SearchResultItem("Yayımcılıkta Telif Hakları Sempozyumu Hk.", "Duyurular"));
            searchableItems.add(new SearchResultItem("İTO BORÇ SORGULAMA / ÖDEME (YENİ) ", "Duyurular"));
            searchableItems.add(new SearchResultItem("Tehlikesiz Atık Üreticilerinin Beyan Zorunluluğu Hk.", "Duyurular"));

            searchableItems.add(new SearchResultItem("Borc Durumu Sorgulama", "Online İşlemler"));
            searchableItems.add(new SearchResultItem("Etkinliğe Kayıt Olma", "Online İşlemler"));
            searchableItems.add(new SearchResultItem("Vize Sorgulama", "Online İşlemler"));
            searchableItems.add(new SearchResultItem("Tescil Başvuru Sonucu Sorgulama", "Online İşlemler"));

            searchableItems.add(new SearchResultItem("JEC WORLD 2016", "Fuarlar"));
            searchableItems.add(new SearchResultItem("MİPİM 2016", "Fuarlar"));
            searchableItems.add(new SearchResultItem("Canton 2016", "Fuarlar"));
            searchableItems.add(new SearchResultItem("Hannover 2016", "Fuarlar"));
            searchableItems.add(new SearchResultItem("SIAL CHINA 2016", "Fuarlar"));
            searchableItems.add(new SearchResultItem("Automechanica 2016", "Fuarlar"));
            searchableItems.add(new SearchResultItem("IFAT 2016", "Fuarlar"));
            searchableItems.add(new SearchResultItem("GANA - 1. Türk Ürünleri Fuarı", "Fuarlar"));
        }

        ArrayList<HeadlinesDTO> list = new ArrayList<>();
        list.add(new HeadlinesDTO(R.drawable.headline1,
                "Çağlar: Türkiye, İran’ın en güclü partneri olacak",
                "İstanbul Ticaret Odası (İTO) Yönetim Kurulu Başkanı İbrahim Çağlar, İran’la ticari ilişki sürecini uzun zamandır yakından izlediklerini belirterek, buna uygun olarak proaktif girişimlerde bulunduklarını acıkladı.\n" +
                        "\n" +
                        "Çağlar, İran’ın özellikle bankalarına ve ana endüstrilerine uygulanan ambargonun kalkmasının dünya barışı ve Türkiye-İran ekonomik ilişkilerinde yeni bir şafak vakti olduğunu söyledi.\n" +
                        "\n" +
                        "MAYISTA TÜRK ÜRÜNLERİ FUARI\n" +
                        "\n" +
                        "Gectiğimiz ay İran Odalar Birliği Başkanı ve beraberindeki 50 iş adamını İTO’da ağırladıklarını hatırlatan Çağlar, “Oda olarak uzun zamandır İran’ı mercek altına aldık. Ambargonun kalkacağını öngörerek bir yıl önce Tahran’da büyük ses getiren hazır giyim ve konfeksiyon fuarı düzenledik. İran Ekonomi Bakanı’nın katıldığı bir yuvarlak masa toplantısı yaptık. Her iki tarafın iş adamlarının da katıldığı üc ayrıca seminer gercekleştirdik. Önümüzdeki mayıs ayında da İran’da genel ticaret alanında bir Türk Ürünleri Fuarı (Expo Turkey in Iran) organize etmekteyiz. İnanıyorum ki bu fuar, yatırımcılarımız icin önemli bir imkan oluşturacak” dedi.\n" +
                        "\n" +
                        "ORTAK ÜRETİM YAPALIM\n" +
                        "\n" +
                        "Türkiye’nin İran ile ticaretini her zaman sürdürdüğünü belirten Çağlar, şunları söyledi: “İran’la bugüne kadar devam eden ticaretimizi ve karşılıklı işbirliğimizi yeni bir boyuta taşımalıyız. İran’ın şu anda en önem verdiği konu, kendi ülkesine yatırım yapılması. İran Cumhurbaşkanı Sayın Ruhani gelecek yıllarda 50 milyar dolarlık uluslararası yatırım cekmeyi hedeflediklerini acıkladı. İran’ın 32.6 milyar dolarlık varlıklarının da serbest bırakılması söz konusu. Türkiye’nin İran’la hem tarihi hem ekonomik ilişkileri düşünüldüğünde en güclü iş ortağının Türkiye olacağını düşünüyoruz. İstanbullu tüccar olarak hedefimiz İran’a hem mal satmak hem de o ülkenin girişimcisiyle ortak üretim yapıp, ücüncü ülkelere acılmak. Her iki tarafın da kazanacağı bircok fırsat önümüzde. İran’da ortak yatırım alanları belirleyip üretime başlamak icin bir an önce kolları sıvamamız gerekiyor.”\n" +
                        "\n" +
                        "2015’TE İRANLI 240 YENİ FİRMA KURULDU\n" +
                        "\n" +
                        "İstanbul Ticaret Odası kayıtlarına göre İstanbul’da 2015’te İranlı yatırımcılar tarafından 240 yeni firma kuruldu. İstanbul’da halen İTO’ya üye İran sermayeli 1.715 firma faaliyet gösteriyor. Bu firmalar icinde gectiğimiz yıl kurulan 240 firmanın en fazla ilgi gösterdiği sektör 48 firma ile ‘toptan ve dış ticaret’ oldu."));
        list.add(new HeadlinesDTO(R.drawable.headline2,
                "İstanbul tüccarı tek yürek",
                "İstanbullu 390 bin firmayı temsil eden İstanbul Ticaret Odası (İTO) Meclis Üyeleri, önceki gün Sultanahmet'te ve bugün Diyarbakır'da gercekleşen alcak terör saldırılarında hayatını kaybedenleri anmak ve terörü lanetlemek üzere Sultanahmet Dikilitaş önünde bir araya geldi.\n" +
                        "\n" +
                        "Meclis Başkanı Şekib Avdagic ve İTO Yönetim Kurulu Başkanı İbrahim Çağlar'ın da bulunduğu heyet, Dikilitaş önünde karanfil bıraktılar.\n" +
                        "\n" +
                        "İstanbul Ticaret Odası Başkanı İbrahim Çağlar, Sultanahmet'te ve Diyarbakır'da gercekleşen alcak terör saldırılarını nefretle lanetlediklerini söyledi. Çağlar, şunları söyledi: \"Diyarbakır'daki saldırıda daha henüz biri beş aylık diğeri bir yaşında iki bebek ve 5 yaşında bir cocuğumuz öldü. Hangi emel, hangi hedef, hangi siyasi amac beş aylık bir cocuğun canından daha değerli olabilir? Hangi ideal bir bebeğin saflığından daha temiz olabilir?\n" +
                        "\n" +
                        "Ülkemizi aşağıya cekmek isteyenler farklı sebepleri bahane ederek yıllardır bu oyunları sahneye koyuyor. Bu ülkeyi bileğinin gücüyle, aklıyla, cesaretiyle deviremeyenler, arkadan dolaşarak, kalleşce oyunlarla bizi zora düşürmeye cabalıyor. Buradan sesleniyorum... Teröre destek veren her kişi, kurum ya da ülke döktükleri kanın hesabını vermekten kacamayacaklar!\n" +
                        "\n" +
                        "Bir yandan hendek kazarken, diğer yandan da masum bebeklerin canına kastedenler bilmelidirler ki; bu kalleşce eylemler ancak ve ancak terörle mücadeledeki azmimize ve kararlığımıza hizmet eder. Saldırılarda ölenleri rahmetle anıyor, yaralılara da acil şifalar diliyorum.\" "));
        list.add(new HeadlinesDTO(R.drawable.headline3,
                "İstanbul Ticaret Odası’ndan duyuru",
                "Sayın İstanbul Ticaret Odası Üyesi;\n" +
                        "\n" +
                        "İnternette ve sosyal medyada ‘İTO Genclik Grubu’ ismiyle etkinliklerini paylaşan ve SMS mesajları ileten oluşumla Odamız’ın ilgisi bulunmamakta olup, Odamız’ın tüm duyurularına ve sosyal medya hesaplarına İTO’nun resmi internet sitesi olan www.ito.org.tr adresinde yer verilmektedir.\n" +
                        "\n" +
                        "Bilgilerinize saygıyla sunarız.\n" +
                        "İstanbul Ticaret Odası"));
        list.add(new HeadlinesDTO(R.drawable.headline4,
                "İTO'nun Yaşayan İstanbul maketi İstanbullularla buluştu",
                "İstanbul maketi, 4-8 Kasım 2015 tarihlerinde kapılarını acacak CNR EMLAK 2015 Fuarı'nda tanıtılıyor. Şehir maketi üc ay boyunca ana fuaye alanında bulunacak.\n" +
                        "\n" +
                        "Fransa'nın Cannes kentinde gectiğimiz Mart ayından düzenlenen dünyanın en büyük gayrimenkul fuarı MIPIM'de ilk kez sergilenen İstanbul maketi, 'istanbul@MIPIM2015' sloganıyla dünyanın dört bir yanından gelen gayrimenkul profesyonellerine sunulmuştu.\n" +
                        "\n" +
                        "İstanbul Ticaret Odası 96 metrekarelik İstanbul maketini, kentin tanıtımına katkıda bulunmak amacıyla dünyanın prestiijli fuarlarında ve ayrıca İstanbul'da yeni mekanlarda sergilenmesi icin de calışmalarını sürdürüyor.\n" +
                        "\n" +
                        "YAŞAYAN İSTANBUL MAKETİ\n" +
                        "\n" +
                        "    96 metrekarelik büyüklüğü ile bugüne kadar yapılmış en büyük İstanbul maketi olarak tasarlanan İstanbul maketi, video haritalama teknolojisinin kullanıldığı görüntü efektleriyle İstanbul’un 24 saatini yaşatıyor.\n" +
                        "    Maket gecesiyle, gündüzüyle, boğazda yüzen gemisiyle, yaşayan İstanbul'un bin 440 dakikasını 5'er dakikalık şovlarla izlettirebiliyor.\n" +
                        "    Projeleri, kültürel ve tarihi mirası ile İstanbul'u görsel acıdan sunan maket, şehrin marka değerine de katkı sağlıyor.\n" +
                        "\n"));
        list.add(new HeadlinesDTO(R.drawable.headline5,
                "365 gün İstanbul turizmi ",
                "İstanbul turizmine yeni destinasyonlar kazandırmak için önemli bir çalışmanın startı verildi. İstanbul’un turizm iletişim stratejisi baştan aşağı yeniden yazılıyor.\n" +
                        "\n" +
                        " \n" +
                        "\n" +
                        "İstanbul Büyükşehir Belediye (İBB) Başkanı Kadir Topbaş’ın başkanlığında belirli periyotlarla “İstanbul Turizmi Zirvesi” toplanacak. Turizmden sorumlu İstanbul Vali Yardımcısı İsmail Gültekin, İstanbul Ticaret Odası Başkanı İbrahim Çağlar, İBB Genel Sekreteri Hayri Baraçlı, Türk Hava Yolları (THY) Genel Müdürü Temel Kotil, Turistik Otelciler, İşletmeciler ve Yatırımcılar Birliği (TUROB) Başkanı Timur Bayındır, Türkiye Seyahat Acentaları Birliği (TURSAB) Başkanı Başaran Ulusoy, İBB Başkan Danışmanı Tülin Ersöz’den oluşan çalışma grubunun ilk ve öncelikli hedefi İstanbul’a yeni destinasyonlar kazandırmak olacak.\n" +
                        "\n" +
                        "YURT DIŞINDA TANITIM\n" +
                        "\n" +
                        "Ortak akıl ve işbirliği temelinde sürdürülecek çalışmalarda İstanbul’un kongre turizmindeki etkinliğinin de artırılması hedefleniyor. Başkan Topbaş başkanlığında yapılan ilk toplantıda, İstanbul’un yurt dışında tanıtımına yönelik atılacak adımlar ele alındı. Buna göre Valilik, İstanbul Büyükşehir Belediyesi, THY, TUROB ve TURSAB’ın destekleriyle İstanbul’un yurt dışında tanıtımına yönelik yenilikçi ve işlevsel çalışmalar yapılacak.\n" +
                        "\n" +
                        " \n" +
                        "\n" +
                        "*        *        *\n" +
                        "\n" +
                        " \n" +
                        "\n" +
                        "‘İstanbul’a yeni destinasyonlar kazandıracağız’\n" +
                        "\n" +
                        " \n" +
                        "\n" +
                        "İstanbul Büyükşehir Belediye Başkanı Kadir Topbaş, “2004’te İstanbul Büyükşehir Belediye Başkanı olarak göreve başladığımda yaptığım ilk iş turizm sektörü temsilcileriyle bir araya gelip, yol haritası hazırlamak olmuştu. Sektör temsilcileriyle birlikte yaptığımız çalışmalar neticesinde İstanbul turizmi önemli ölçüde gelişme gösterdi. Şimdi bunu daha da yukarıya taşımak istiyoruz. Çalışmalarımızı ortak akıl ve işbirliği temelinde sürdüreceğiz. İstanbul’a yeni destinasyonlar kazandırma noktasında çok hızlı sonuç alacağımıza inanıyorum” diye konuştu.\n" +
                        "\n" +
                        " \n" +
                        "\n" +
                        "*        *        *\n" +
                        "\n" +
                        " \n" +
                        "\n" +
                        "İstanbul, ‘İki Şehrin Hikayesi’ ile uluslararası medyada\n" +
                        "\n" +
                        " \n" +
                        "\n" +
                        "İstanbul Kongre ve Ziyaretçi Bürosu’nun (ICVB) İstanbul’un tanıtımıyla ilgili çalışmaları uluslararası medyada ses getirmeye devam ediyor. Almanya ve Belçika’nın ardından İngiltere ve Hindistan’ın kongre sektörünün önde gelen dergileri de İstanbul’un tanıtımına geniş yer ayırdı. İngiltere’de aylık 18 bin 500 sektör profesyoneline ulaşan C&IT Magazine, İstanbul gezisine geniş yer verdi. 3 ayda bir yayınlanan ve 10 bin tirajı olan CMW Dergisi’nde yer alan “İstanbul basın gezisi 2015” başlıklı haberde ise ICVB’nin mart sonunda uluslararası medya temsilcilerini İstanbul’da ağırladığı, kongre merkezleri, oteller ve diğer önemli yerlerin gündemi oluşturduğu belirtildi.\n" +
                        "\n" +
                        " \n" +
                        "\n" +
                        "İngiltere’nin önde gelen kongre dergilerinden Business Destinations de, İstanbul’dan övgüyle bahsederek 3 tam sayfa ayırdı. Dergide, “Kıtaları buluşturan marka şehir İstanbul’un uluslararası önemli toplantılara ev sahipliği yaptığı” vurgulandı. Hindistan’da 12 bin tirajlı Experiential Marketing (EXM) ise İstanbul’a 4 tam sayfa ayırdı. “İki Şehrin Hikayesi” başlığıyla verilen haberde, İstanbul’un “modernite ve gelenek, batı ve doğu, dinamizm ve huzur” gibi farklı yüzlerine dikkat çekildi. "));
        list.add(new HeadlinesDTO(R.drawable.headline6,
                "En çok ‘Tax Free’ alışverişi komşu İran yaptı",
                "Global Blue Türkiye’nin verilerine göre senenin ilk 6 ayında yapılan Tax Free (turistler için vergisiz) alışveriş, geçen senenin aynı dönemine oranla yüzde 42 büyüdü. Bu alışverişlerden en çok İranlılar yararlanıyor. Ardından Araplar, Azeriler ve Ruslar geliyor.\n" +
                        "\n" +
                        " \n" +
                        "\n" +
                        "PERAKENDE gelirlerinde yabancıların önemli katkısı olduğunu vurgulayan Global Blue Türkiye Ülke Müdürü Selim Şeyhun, “Tax Free sıralamasında ilk 5 içinde yer alan ülkelerin büyümeleri karşılaştırıldığında İran yüzde 57, Suudi Arabistan yüzde 147, Azerbaycan yüzde 3 ve Kuveyt yüzde 48 büyürken; içinde bulunduğu ekonomik bunalım nedeniyle Rusya yüzde 13 küçüldü. Geçen sene 3’üncü sırada olan Rusya, bu sene 5’inci sıraya geriledi” açıklamalarında bulundu.\n" +
                        "\n" +
                        "Tax Free alışverişte 2015’in Haziran ayında 2014 Haziran ayına göre yüzde 48 büyüme elde edildi. Şeyhun, bu büyümede İstanbul Shopping Fest’in yaz dönemine denk gelmesinin, turist sayısının artmasının ve indirimlerin başlamasının rolü olduğunu belirtti.\n" +
                        "\n" +
                        "12 BİN NOKTADA VERGİSİZ\n" +
                        "\n" +
                        "Şeyhun, dünyada vergisiz alışverişin yüzde 33’ünü Çinliler’in yaptığını ifade etti. Türkiye’de Çinliler’in payının yüzde 5 seviyelerinde olduğunu bildirdi. Selim Şeyhun, “2015 Haziran ayı ile 2014 Haziran ayı verilerine bakıldığında Çinliler’in yaptığı alışveriş yüzde 213 büyüdü. Yakın vadede perakendenin en büyük alıcılarından birisi Çinliler olacaktır” dedi.\n" +
                        "\n" +
                        "Tek alışverişte ortalama 2 bin 363 TL ile en büyük harcamayı Katarlılar yapıyor. Onları bin 978 TL ile gurbetçiler takip ediyor. Ardından bin 686 TL harcama ile Azerbaycan, bin 643 TL harcama ile Arap Emirlikleri ve bin 433 TL harcama ile Çinliler izliyor.\n" +
                        "\n" +
                        " \n" +
                        "\n" +
                        "9 havalimanı ve AVM’ler ile yapılan ortaklıklarla 30’a yakın şehir içi nakit iade bürosu, sınır kapıları ve limanlara hizmet sunduklarını anlatan Selim Şeyhun, turistlerin 12 bin satış noktasından Tax Free hizmeti alabileceğini sözlerine ekledi."));
        list.add(new HeadlinesDTO(R.drawable.headline7,
                "Bu endeks ticaretin nabzını ölçüyor",
                "İstanbul Ticaret Odası ile İstanbul Ticaret Üniversitesi, Türkiye’de ticari faaliyet eğilimlerini ortaya koyan Ticari Faaliyet Beklenti Endeksi’ni hayata geçirdi. Veriler aylık bazda değerlendirilip açıklanıyor.\n" +
                        "\n" +
                        "İSTANBUL Ticaret Odası (İTO) ile İstanbul Ticaret Üniversitesi Ekonomi ve Finans Uygulama ve Araştırma Merkezi’nce yürütülen ortaklaşa bir çalışmayla oluşturulan Ticari Faaliyet Beklenti Endeksi (TFE), iş dünyasına, akademisyenlere ve analistlere İstanbul’daki ticari faaliyet eğilimleri hakkında önemli bilgiler sağlıyor. Bir yıldan uzun bir süredir hesaplanan endeks, ekonomik durumun hangi yönde değişeceğine dair bir ön veri sunuyor. Benzer bir endeks olan Satın Alma Yöneticileri Endeksi (veya Purchasing Managers’ Index-PMI), tüm dünyada piyasalar tarafından yakından takip ediliyor.\n" +
                        "EKONOMİ İÇİN GÖSTERGE\n" +
                        "İstanbul’un ticari faaliyetleri hakkındaki bilgiler yalnız İstanbul için değil, tüm Türkiye ekonomisi için bir gösterge niteliğinde. Bu nedenle, TFE, Türkiye’de iş yapan veya Türkiye ekonomisini takip eden tüm taraflara çok değerli bir bilgi sunuyor.\n" +
                        "TFE NEDİR?\n" +
                        "TFE, İstanbul’da ticari faaliyetler konusundaki eğilimleri rakamsallaştırarak özetleyen bir öncü gösterge. Bir başka deyişle TFE, ticari faaliyetlerde beklenen canlılığa veya durgunluğa ve bu canlılık veya durgunluğun ne ölçüde olacağına işaret ediyor.\n" +
                        "NASIL HESAPLANIYOR?\n" +
                        "İTO’ya kayıtlı firma temsilcileriyle yapılan anketlerde katılımcılara, üretim, yatırım, satış, sipariş, ithalat-ihracat ve istihdam gibi konularda beklentileri soruluyor. Sorulara “artış”, “azalış” veya “bir değişiklik beklemiyoruz” olarak cevap vermeleri isteniyor. Anket sonuçları yayılma endeksleri yöntemiyle hesaplanıyor. Endeksin 50 değerinin üzerinde olması bu değerde artış olduğunu, 50 değerinin altında olması bu değerde düşüş olduğunu gösteriyor.\n" +
                        "Anketler İTO’ya kayıtlı tüm firmaların temsil edildiği 81 meslek komitesinin 570’in üzerinde sektör temsilcisine uygulanıyor.\n" +
                        "ENDEKSİN AVANTAJLARI\n" +
                        "TFE sayesinde ticari faaliyet eğilimleri hakkında öncü bilgiler edinen firmalar üretim, ticaret ve finansman gibi faaliyetlerini önceden koordine ederek daha verimli çalışma imkanına sahip olabiliyor. Analistler ise ticari ve ekonomik eğilimleri önceden görerek genel ekonomik beklentilerini oluştururken daha fazla bilgi ediniyor. Genel Ticari Faaliyet Beklenti Endeksi, üretim, ticaret ve hizmet alt sektörleri için oluşturulan endekslerin ağırlıklandırılmış toplamını veriyor.\n" +
                        "PARALEL SEYİR\n" +
                        "İTO’nun aylık meclis toplantılarında meclis üyeleriyle paylaşılan TFE’nin, genel ekonomik gidişatla paralellik gösterdiği görülüyor. Bunun yanında, TFE sonuçları; TÜİK-TCMB/Tüketici Güven Endeksi, CNBC-e/Tüketici Güven Endeksi, TCMB/ Reel Kesim Güven Endeksi, MUSİAD/SAMEKS (Satın Alma Müdürleri Endeksi) gibi endekslerle de paralel bir seyir izliyor. Anketlerde yer alan her bir alt gösterge ayrı ayrı incelendiğinde sanayi üretim endeksi, ihracat ve ithalat miktar endeksleri, istihdam, döviz kuru gibi makroekonomik büyüklükler takip edilerek bu veriler arasındaki paralellikler de teyit ediliyor.\n" +
                        "\n" +
                        "*    *    *\n" +
                        "\n" +
                        "Diğer endekslerden farklı\n" +
                        "Ticari Faaliyet Beklenti Endeksi, İstanbul Ticaret Üniversitesi Ekonomi ve Finans Araştırma ve Uygulama Merkezi bünyesinde titizlikle hazırlanıyor. Merkez Müdürü Doç. Dr. Elçin Aykaç Alp ve Araştırma Görevlisi Mete Han Yağmur, öğrencilerle birlikte anket sonuçlarını büyük bir dikkatle değerlendiriyor ve sonuçları açıklıyor. Doç. Dr. Elçin Aykaç Alp, endeksin farkını şu cümlelerle ortaya koydu: “Benzer endekslerden farklı olarak 4 kategorisi var. Bir genel endeks, üretim, ticaret ve hizmet olmak üzere de üç alt başlığı var. Ticaret kategorisinde bir endeks hesaplıyor olmamız da ayrıca farklılaşan yönlerden biri. Faaliyet gösterenlerin ticari eğilimleri ve ekonomiye ilişkin beklentileri ölçülüyor. Ticari faaliyetlerde içinde bulunduğumuz ay, gelecek ay ve hatta gelecek üç ayda bir daralma mı olacak, sabit mi kalacağız, yoksa bir genişleme mi olacak; bunu ölçüyoruz. Alt kalemlerde sorduklarımız birer öncü gösterge özelliğini taşıyor.”\n" +
                        "\n" +
                        "*    *    *\n" +
                        "\n" +
                        "Anketler haftalık açıklama aylık\n" +
                        "Anketlerin haftalık periyotlarla yapıldığına dikkat çeken Doç. Dr. Elçin Aykaç Alp, “Bilgiler öğrenciler tarafından bilgisayar ortamına giriliyor. Tüm anketler toplandıktan sonra ay sonunda değerlendiriliyor. Biz anketi tüm kitleyi en iyi temszil edenlere uyguluyoruz” dedi."));
        list.add(new HeadlinesDTO(R.drawable.headline8,
                "İklim zararına risk transferi",
                "Dünya ile birlikte ülkemizde de iklim değişikliğinin olumsuz etkileri yaşanıyor. Günlük hava olayları da zirai alanlarda ciddi zarar veriyor. Uzmanlar, tarladaki zararın ticaret ve ekonomiyi olumsuz etkilememesi için ekim alanları stratejisi, meteorolojik veri alternatifleri ve tarım sigortası kullanımını öneriyor.\n" +
                        "\n" +
                        "ADEM ORHUN\n" +
                        "\n" +
                        "TÜRKİYE, haziran ayını pek de alışık olmadığı şekilde yağışlarla geçirdi. Mayıs sonunda Samsun, Amasya, Nevşehir’de yaşanan dolu yağışı tarla ürünlerine ve meyve ağaçlarına zarar verdi. Adana, Yozgat ve Kütahya’daki aşırı yağışlar sebebiyle tarlalar su altında kaldı. Bilecik ve Sakarya’da da benzer manzaralar sebze ve meyve üreticilerini üzdü.\n" +
                        "Ziraatle uğraşanlar ürün zararından şikayetçi olurken, temmuza kadar sürekli kapalı ve serin hava, vatandaş tarafında ‘bu sene yaz yok’ yorumlarına sebep oldu. İstanbul Ticaret Gazetesi olarak hava koşullarını, iklimi, tarladaki ve raflardaki ürünün durumunu öğrenmek için konunun ilgilileriyle görüştük.\n" +
                        "Çevre ve Şehircilik Bakanlığı Müsteşar Yardımcısı ve İklim Değişikliği Başmüzakerecisi Prof. Dr. Mehmet Emin Birpınar’a ilettiğimiz sorular Bakanlık tarafından ayrıntılı bir şekilde yanıtlandı.\n" +
                        "MUSON İKLİMİ DEĞİL\n" +
                        "Haziran ayındaki yağışları değerlendiren Bakanlık yetkilileri, “2015’teki aşırı yağışlara bakarak bundan sonra her yılın böyle geçeceğini düşünmek mümkün değil. Bu yağışların çoğu muson etkisinden ziyade konvektif (dikine hava hareketleri) faaliyetlerinden kaynaklandı” yorumunu yaptı. Bununla birlikte yetkililer, dikine hareketlerin sonucu olarak Türkiye’de son yıllarda hortum, yıldırım, dolu ve sel olaylarının da arttığına dikkat çekti.\n" +
                        "Bakanlık yetkililerinin verdiği bilgiye göre tüm dünya ile birlikte ülkemizde de iklim değişikliğinin olumsuz etkileri yaşanıyor. Türkiye’de son 45 yılda ortalama sıcaklıklar 1 derece arttı. Ortalama sıcaklıklar 1994 yılından bu yana (1997 ve 2011 hariç) normal seyrindeydi. 2010 yılı ise en sıcak yıl olarak kayıtlara geçti.\n" +
                        "ULUSAL EYLEM PLANI\n" +
                        "İklim değişikliği ile mücadele amacıyla Çevre ve Şehircilik Bakanlığı koordinasyonunda Ulusal İklim Değişikliği Stratejisi (2010-2020) hazırlandı. Stratejinin uygulanması için de İklim Değişikliği Ulusal Eylem Planı (2011-2023) hazırlanarak uygulamaya konuldu. Ayrıca Gıda Tarım ve Hayvancılık Bakanlığı Tarım Reformu Genel Müdürlüğü koordinasyonunda hazırlanan Türkiye Tarımsal Kuraklıkla Mücadele Stratejisi ve Eylem Planı (2013-2017) dahilinde de çalışmalar yapılıyor.\n" +
                        "ÜRETİCİLER NE YAPMALI\n" +
                        "Bakanlık yetkilileri, kurak ve yağışlı mevsimlere göre üreticinin farklı şekilde pozisyon alması gerektiğini vurguladı. Üreticilerin uyarıları dikkate alması ve zarardan korunma yöntemlerini uygulaması gerektiğini hatırlatan yetkililer şunları kaydetti: “Değişen iklimle birlikte atmosferde meydana gelen aşırı olayların frekans ve şiddetinde artış olacak. Bu yüzden Türkiye’nin artık şiddetli yağışlar, kuraklık, sıcak hava dalgaları gibi aşırı olaylara hazırlıklı olmasında fayda var. Her sektörün uyum, korunma, önleme, risk transferi (sigorta) yöntemlerini harekete geçirmesi gerekir.”\n" +
                        "\n" +
                        "*    *    *\n" +
                        "\n" +
                        "Çeltikte rekor bekleniyor\n" +
                        "İTO Hububat, Bakliyat, Kuruyemiş ve Kuru Meyve Meslek Komitesi Başkanı Hakkı İsmet Aral, “Ocak ayından itibaren yağışların iyi olması buğdayda ürünü artırdı. Bu vakitten sonra yağmurun olumlu katkısı olmaz. Buğday rekoltesine bakacak olursak Konya’dan iyi haberler geliyor. Kırmızı mercimekte de durum iyi, 350 bin ton mahsul bekleniyor. Nohut rekoltesi de fena çıkmayacak gibi görünüyor. Fasulye ekimleri fiyat düştüğü için azalmış olsa da mahsul iyi. Çeltikte ise durum son derece iyi. 700 bin tondan 950 bin tona çıkar diye bekliyoruz. Eylül ayında göreceğiz” dedi.\n" +
                        "\n" +
                        "*    *    *\n" +
                        "\n" +
                        "Haziran yağışları Trakya’ya yaradı\n" +
                        "Son haftalardaki hava olaylarının Trakya’daki hububat ekim alanlarına etkisini öğrenmek için görüştüğümüz Tekirdağ Merkez Süleymanpaşa Ziraat Odası yetkilileri, “Bölgemizde buğday, kanola ve ayçiçeği ekimi yapılıyor. Yaz başındaki yağışlar tarladaki ürüne iyi geldi. Geçen ay yaşanan dolu 100 bin dönümde zarara yol açtı. Doludan etkilenen çiftçilerin yüzde 50’si TARSİM sigortalıydı. 4 milyon dönüm ekili arazinin bulunduğu Tekirdağ’da ürün gelişimi iyi. Ancak bundan sonra güneş bekliyoruz. Yağarsa, hububatı olumsuz etkiler. Kanola hasatı başladı, bu hafta buğday başlayacak. Daha sonra yazlıklara (bostana) geçilecek” dedi. Tekirdağ’da kanola ve buğday rekoltesinde hafif düşüş beklendiğini kaydeden Ziraat Odası yetkilileri, Türkiye’nin güney bölgelerinde buğdaydan çok iyi haberler aldıklarını belirtti.\n" +
                        "\n" +
                        "*    *    *\n" +
                        "\n" +
                        "Üstü açık fabrika\n" +
                        "Meteorolojik olaylar tarladaki ürünü ciddi şekilde etkilediği için konuyu İTÜ Öğretim Üyesi, Atmosfer Bilimci ve Afet Yönetim Uzmanı Prof. Dr. Mikdat Kadıoğlu’na da sorduk. “Türkiye’nin artık tropikal ya da muson iklimine girdiğine dair akla, bilime zarar açıklamalar servis ediliyor” diyen Kadıoğlu şunları söyledi: “Örneğin son haftalarda yaşanan aşırı ve ani yaz yağmurlarının bundan sonra her yıl görüleceği, böyle bir iklime girdiğimiz asla söylenmemeli. Günlük hava şartlarıyla iklim çok farklı şeyler” dedi.\n" +
                        "ÜÇ OLAY ETKİLEDİ\n" +
                        "“Üstü açık bir fabrika olan tarım sektörünü bu sene üç farklı hava olayı olumsuz şekilde etkiledi” diyen Prof. Kadıoğlu, şu bilgiyi verdi:\n" +
                        "“Bu olumsuz etkilerden biri kışın kuzey enlemlerden Kuzey Afrika’ya inen Sibirya Ekspresi de denilen soğuk hava dalgalarıydı. Kışın toprak üzerinde -20 dereceye düşen sıcaklık, bitkilerin kök ve gövdelerinde çatlamaya sebep oldu.\n" +
                        "İkinci sebep; 2014-2015 kışında aralıklı olarak yüksek basınç etkisiyle açık-güneşli günler yaşadık. Çiçekler açtıktan sonra nisan ayında yaşanan ayaz sebebiyle pek çok bitki dondu.\n" +
                        "KUZEYLİ HAVA ETKİSİ\n" +
                        "Üçüncüsü; haziran ayında havalar beklenenin dışında serin ve yağışlı geçti. Bunun sebebi de yaklaşık 5 km seviyede kuzeyli soğuk havanın üzerimize oluklar ile inmesidir.”\n" +
                        "DAHA KURAK OLACAK\n" +
                        "Prof. Kadıoğlu, “Türkiye’nin iklim durumunu anlamanın en kolay yolu güneyimize bakmak. Suriye ya da Kuzey Afrika’daki ülkelerin hiçbirinde tropikal ya da musona benzeyen bir iklim bulunmuyor” dedi. “Tropikal veya muson iklimine doğru gidiyor olsak buna çok sevinmemiz gerekirdi” diyen Prof. Kadıoğlu şunları söyledi:\n" +
                        "“Halbuki yarı kurak bir iklime sahip olan ülkemizin uzun vadede daha kurak bir ülke haline gelmesi bekleniyor. Özellikle Akdeniz iklimi bölgesinde en büyük problem kışın yağışların azalmasıdır. Böylece yazları sıcak ve kurak, kışları ılık ve yağışlı diye bildiğimiz Akdeniz iklimi tekerlemesi bile bozuluyor.”\n" +
                        "\n" +
                        "*    *    *\n" +
                        "\n" +
                        "Meteoroloji mühendisi çalıştırın\n" +
                        "Hava şartlarına duyarlı olan organizasyonların, tüccarların ve firmaların gelişmiş ülkelerdeki gibi kendi bünyelerinde meteoroloji mühendisi çalıştırmaları gerektiğini vurgulayan Prof. Dr. Mikdat Kadıoğlu, “Bu bir tavsiye değil zorunluluktur. Böylece televizyonlardaki genel hava tahminleri veya tek bir kaynak yerine, birçok kaynaktan verilerle üretilmiş kendilerine özel ve noktasal hava tahminleri alabilirler. Kısa, orta ve uzun vadeli hava tahminleri çiftçilere gündelik işlerini planlamanın yanı sıra ne ekip biçmesi ve ürününe ne fiyat koyması gerektiğine kadar birçok konuda faydalı olacaktır. Ayrıca bilimsel esaslara göre yönetilen şirketler, rakip firmalara ait bölgelerdeki hava şartlarını da takip eder (ülkelerin tarım stratejilerinde bu yöntem uygulanabilir)” dedi.\n" +
                        "\n" +
                        "*    *    *\n" +
                        "\n" +
                        "Vadiler ve alçak bahçeler\n" +
                        "Hava olaylarından en büyük zararı don çukurları denilen sahiller, vadiler, alçak yerlerdeki bağ ve bahçeler görüyor. Ayazlı günlerde soğuk hava tepelerden ovalara su gibi akar, çukur yerleri doldurur. Prof. Kadıoğlu’nun verdiği bilgiye göre, tarımda dondan korunmanın pasif yöntemlerinden biri dağların tepesi ile vadilere bağ ve bahçe kurmamak. Çok para ediyor diye bir ürünü her yere ekmek yerine tepeler ile ovalar arasındaki ılık kuşak bölgelere ekim dikim yapmak ideal.\n" +
                        "\n" +
                        "*    *    *\n" +
                        "\n" +
                        "Yaz nasıl geçecek?\n" +
                        "Hava durumu tahminleri için farklı servislerin kullanılmasını tavsiye eden Prof. Kadıoğlu şunları söyledi: “Herkese açık bir kaynak olan Güney Doğu Avrupa İklim Değişikliği Merkezi (www.seevccc.rs) verilerine göre yaz aylarında hava sıcaklıkları mevsim normalleri civarında olacak. Sadece Trakya’nın batısında mevsim normallerinin biraz üzerinde, iç ve Doğu Anadolu arasındaki bir bölgemizde de normallerin biraz altında. Yağışlar Türkiye’nin kuzeybatısında, Akdeniz ve Doğu Anadolu’nun doğusunda mevsim normallerinin altında; diğer kısımlarda ise genellikle mevsim normallerinin üzerinde geçmesi bekleniyor.”\n" +
                        "\n" +
                        "*    *    *\n" +
                        "\n" +
                        "Uganda kahveyi nasıl koruyor?\n" +
                        "Küresel iklim değişikliğine uyum için Uganda’daki kahve üretimi stratejilerine dikkati çeken Prof. Mikdat Kadıoğlu şöyle konuştu: “Uganda’da hangi ürünün kaç derecelik bir ısınma sonucu gelecekte nasıl desen değiştireceği, nerelerde üretilmeye devam edileceği belirleniyor. Bu çalışmaya göre ortalama hava sıcaklığının 2 derece artması durumunda üretim bölgelerinin çok küçülerek sadece birkaç noktada kaldığı görülüyor. Uganda hükümeti, bu bölgeleri koruma altına alarak buralara bina yapmayı yasaklamış durumda.”"));
        return list;
    }

    public static ArrayList<EventDTO> getEvents() {
        ArrayList<EventDTO> list = new ArrayList<>();
        list.add(new EventDTO(R.drawable.event1, "08 Mart 2016-10 Mart 2016", "JEC WORLD 2016", "Paris / Fransa"));
        list.add(new EventDTO(R.drawable.event2, "15 Mart 2016-18 Mart 2016", "MİPİM 2016", ""));
        list.add(new EventDTO(R.drawable.event3, "15 Nisan 2016-05 Mayıs 2016", "Canton 2016", "China"));
        list.add(new EventDTO(R.drawable.event4, "25 Nisan 2016-29 Nisan 2016", "Hannover 2016", "Hannover / Almanya"));
        list.add(new EventDTO(R.drawable.event5, "05 Mayıs 2016-07 Mayıs 2016", "SIAL CHINA 2016", "Şanghay / Çin Halk Cumhuriyeti"));
        list.add(new EventDTO(R.drawable.event6, "08 Mayıs 2016-10 Mayıs 2016", "Automechanica 2016", "Dubai / Buenos Aires"));
        list.add(new EventDTO(R.drawable.event7, "30 Mayıs 2016-03 Haziran 2016", "IFAT 2016", "Münih /Almanya"));
        list.add(new EventDTO(R.drawable.event8, "24 Ağustos 2016-27 Ağustos 2016", "GANA - 1. Türk Ürünleri Fuarı", ""));
        return list;
    }
}
