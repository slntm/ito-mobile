package tr.org.ito.app.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import tr.org.ito.app.R;
import tr.org.ito.app.activities.announcement.AnnouncementSingleActivity;
import tr.org.ito.app.activities.announcement.AnnouncementsActivity;
import tr.org.ito.app.activities.events.EventsActivity;
import tr.org.ito.app.activities.headlines.HeadlineSingleActivity;
import tr.org.ito.app.activities.headlines.MainActivity;
import tr.org.ito.app.activities.onlineoperations.OnlineOperationsActivity;
import tr.org.ito.app.helpers.SearchHelper;
import tr.org.ito.app.helpers.SearchResultItem;
import tr.org.ito.app.helpers.SearchResultItemAdapter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Q on 01.02.2016.
 */
public abstract class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {
    private final Handler mDrawerActionHandler = new Handler();
    protected Toolbar               mToolbar;
    protected NavigationView        mDrawer;
    protected DrawerLayout          mDrawerLayout;
    protected ActionBarDrawerToggle mDrawerToggle;
    protected int                   mSelectedId;
    private String TAG = getClass().getSimpleName();
    private SearchView   searchView;
    private MenuItem     searchMenuItem;
    private Dialog       searchResultDialog;
    private long         mSearchQueryTextChangeTime;
    private BaseActivity mContext;
    private boolean      redirect;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this; //TODO: SHITTY
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    protected void init() {
        Log.i(TAG, "Init");

        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        //        mToolbar.setContentInsetsAbsolute(0,0);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        mDrawer = (NavigationView) findViewById(R.id.main_drawer);
        mDrawer.setNavigationItemSelectedListener(this);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        hideDrawer();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (mDrawer != null) {
            //header ı xml'den kaldırmam gerekiyor inflate dediğim için
            View mDrawerHeader = mDrawer.inflateHeaderView(R.layout.common_drawer_header);

            if (mDrawerHeader != null) {
                TextView username = (TextView) mDrawerHeader.findViewById(R.id.username);
                username.setText(sharedPreferences.getString("username", "Elma"));
            }
        }
    }

    protected void hideDrawer() {
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    private void showDrawer() {
        mDrawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        searchView.setOnQueryTextListener(this);
        //
        //        SearchManager searchManager =
        //                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        //        searchView.setSearchableInfo(
        //                searchManager.getSearchableInfo(getComponentName()));

        init2();

        return true;
    }

    private void init2() {
        if (this instanceof MainActivity) {
            mDrawer.getMenu().getItem(0).setChecked(true);
            getSupportActionBar().setTitle("Haberler");
            searchView.setQueryHint("Bütün Başlıklarda Ara");
        } else if (this instanceof AnnouncementsActivity) {
            mDrawer.getMenu().getItem(1).setChecked(true);
            getSupportActionBar().setTitle("Duyurular");
            searchView.setQueryHint("Duyurularda Ara");
        } else if (this instanceof EventsActivity) {
            mDrawer.getMenu().getItem(2).setChecked(true);
            getSupportActionBar().setTitle("Fuarlar");
            searchView.setQueryHint("Fuarlarda Ara");
        } else if (this instanceof OnlineOperationsActivity) {
            mDrawer.getMenu().getItem(3).setChecked(true);
            getSupportActionBar().setTitle("Online İşlemler");
            searchView.setQueryHint("Online İşlemlerde Ara");
        } else if (this instanceof LoginActivity) {
            mDrawer.getMenu().getItem(4).setChecked(true);
            getSupportActionBar().setTitle("Oturum Aç");
            searchMenuItem.setVisible(false);
        } else if (this instanceof SettingsActivity) {
            mDrawer.getMenu().getItem(5).setChecked(true);
            getSupportActionBar().setTitle("Ayarlar");
            searchMenuItem.setVisible(false);
        } else if (this instanceof ContactActivity) {
            mDrawer.getMenu().getItem(6).setChecked(true);
            getSupportActionBar().setTitle("İletişim");
            searchMenuItem.setVisible(false);
        } else if (this instanceof HeadlineSingleActivity) {
            getSupportActionBar().setTitle("Haberler");
            searchMenuItem.setVisible(false);
        } else if (this instanceof AnnouncementSingleActivity) {
            getSupportActionBar().setTitle("Duyurular");
            searchMenuItem.setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        showSearchDialog(query);
        return true;
    }

    private void showSearchDialog(final String query) {
        searchResultDialog = new Dialog(this);
        searchResultDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchResultDialog.setContentView(R.layout.dialog_search);

        LinkedHashMap<String, ArrayList<SearchResultItem>> result = SearchHelper.getSearchResults(query, String.valueOf(getSupportActionBar().getTitle()));

        ListView listView = (ListView) searchResultDialog.findViewById(R.id.listView1);
        listView.setAdapter(new SearchResultItemAdapter(this, result));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                List<SearchResultItem> item = (List<SearchResultItem>) parent.getItemAtPosition(position);

                Intent intent = null;
                intent = new Intent(mContext, item.get(0).launchClass);
                //
                //                if(item.get(0).title.equals("Haberler"))
                //                    intent = new Intent(mContext, MainActivity.class);
                //                if(item.get(0).title.equals("Duyurular"))
                //                    intent = new Intent(mContext, AnnouncementsActivity.class);
                //                if(item.get(0).title.equals("Fuarlar"))
                //                    intent = new Intent(mContext, EventsActivity.class);
                //                if(item.get(0).title.equals("Online İşlemler"))
                //                    intent = new Intent(mContext, OnlineOperationsActivity.class);

                intent.putExtra("search", query);

                if (item.get(0).category.equals("Haberler")) {
                    ((MainActivity) mContext).mAdapter.filter(query);
                    redirect = true;
                    searchResultDialog.dismiss();
                } else startActivity(intent);
            }
        });

        final Window window = searchResultDialog.getWindow();
        window.setGravity(Gravity.TOP);
        window.setLayout(searchView.getWidth(), WindowManager.LayoutParams.WRAP_CONTENT);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        int[] searchViewPos1 = new int[2];
        mToolbar.getLocationOnScreen(searchViewPos1);

        WindowManager.LayoutParams WMLP = searchResultDialog.getWindow().getAttributes();

        final TypedArray styledAttributes = getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
        WMLP.y = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        searchResultDialog.show();
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (redirect) ((MainActivity) mContext).mAdapter.filter("");

        return false;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        menuItem.setChecked(true);
        mSelectedId = menuItem.getItemId();

        mDrawerLayout.closeDrawer(GravityCompat.START);
        mDrawerActionHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate(mSelectedId);
            }
        }, 200);
        return true;
    }

    protected void navigate(int mSelectedId) {
        Intent intent = null;
        if (mSelectedId == R.id.navigation_item_1) {
            intent = new Intent(this, MainActivity.class);
        }
        if (mSelectedId == R.id.navigation_item_2) {
            intent = new Intent(this, AnnouncementsActivity.class);
        }
        //        if (mSelectedId == R.id.navigation_item_3) {
        //            intent = new Intent(this, EventsActivity.class);
        //        }
        if (mSelectedId == R.id.navigation_item_4) {
            intent = new Intent(this, EventsActivity.class);
        }
        if (mSelectedId == R.id.navigation_item_5) {
            intent = new Intent(this, OnlineOperationsActivity.class);
        }
        if (mSelectedId == R.id.navigation_item_6) {
            intent = new Intent(this, LoginActivity.class);
        }
        if (mSelectedId == R.id.navigation_item_7) {
            intent = new Intent(this, SettingsActivity.class);
        }
        if (mSelectedId == R.id.navigation_item_8) {
            intent = new Intent(this, ContactActivity.class);
        }
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        //        hideDrawer();
        super.onResume();
    }
}
