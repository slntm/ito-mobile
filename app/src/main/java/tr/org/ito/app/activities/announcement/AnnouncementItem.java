package tr.org.ito.app.activities.announcement;

/**
 * Created by scokceken on 31.01.2016.
 */
public class AnnouncementItem {
    private String mName;
    private String mDate;

    private Class mLaunchClass;

    public AnnouncementItem(String mName, String date) {
        this.mName = mName;
        this.mDate = date;
//        this.mLaunchClass = mLaunchClass;
    }

    public String getName() {
        return mName;
    }

    public Class getLaunchClass() {
        return mLaunchClass;
    }

    public String getDate() {
        return mDate;
    }
}
