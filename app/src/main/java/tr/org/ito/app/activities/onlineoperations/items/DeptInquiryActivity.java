package tr.org.ito.app.activities.onlineoperations.items;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import tr.org.ito.app.R;
import tr.org.ito.app.activities.BaseActivity;

public class DeptInquiryActivity extends BaseActivity {
    private EditText inputTCKN, inputPassword;
    private TextInputLayout inputLayoutTCKN, inputLayoutPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dept_inquiry);
        init();

        inputLayoutTCKN = (TextInputLayout) findViewById(R.id.input_layout_TCKN);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password);
        inputTCKN = (EditText) findViewById(R.id.input_TCKN);
        inputPassword = (EditText) findViewById(R.id.input_password);
        Button btnSignUp = (Button) findViewById(R.id.btn_login);

        inputTCKN.addTextChangedListener(new MyTextWatcher(inputTCKN));
        inputPassword.addTextChangedListener(new MyTextWatcher(inputPassword));

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
    }

    /**
     * Validating form
     */
    private void submitForm() {
        if (!validateTCKN()) {
            return;
        }

        if (!validatePassword()) {
            return;
        }

        Toast.makeText(getApplicationContext(), "Thank You!", Toast.LENGTH_SHORT).show();
    }

    private boolean validateTCKN() {
        String TCKN = inputTCKN.getText().toString().trim();

        if (TCKN.isEmpty() || !isValidTCKN(TCKN)) {
            inputLayoutTCKN.setError(getString(R.string.err_msg_TCKN));
            requestFocus(inputTCKN);
            return false;
        } else {
            inputLayoutTCKN.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (inputPassword.getText().toString().trim().isEmpty()) {
            inputLayoutPassword.setError(getString(R.string.err_msg_password));
            requestFocus(inputPassword);
            return false;
        } else {
            inputLayoutPassword.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidTCKN(String TCKN) {
        return !TextUtils.isEmpty(TCKN);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {
        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_TCKN:
                    validateTCKN();
                    break;
                case R.id.input_password:
                    validatePassword();
                    break;
            }
        }
    }
}
