package tr.org.ito.app.activities.onlineoperations;

/**
 * Created by scokceken on 31.01.2016.
 */
public class OnlineOperationItem {
    private String mName;
    private String mDescription;
    private boolean mIsFavorite;
    private Class mLaunchClass;

    public OnlineOperationItem(String mName, String mDescription, Class mLaunchClass, Boolean mIsFavorite) {
        this.mName = mName;
        this.mDescription = mDescription;
        this.mLaunchClass = mLaunchClass;
        this.mIsFavorite = mIsFavorite;
    }

    public String getName() {
        return mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public boolean isIsFavorite() {
        return mIsFavorite;
    }

    public void setIsFavorite(boolean mIsFavorite) {
        this.mIsFavorite = mIsFavorite;
    }

    public Class getLaunchClass() {
        return mLaunchClass;
    }
}
