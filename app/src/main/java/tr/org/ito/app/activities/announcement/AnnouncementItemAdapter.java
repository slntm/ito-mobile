package tr.org.ito.app.activities.announcement;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import tr.org.ito.app.R;
import tr.org.ito.app.activities.BaseActivity;
import tr.org.ito.app.dtos.AnnouncementSingleDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by scokceken on 31.01.2016.
 */
public class AnnouncementItemAdapter extends RecyclerView.Adapter<AnnouncementItemAdapter.ViewHolder> {
    List<AnnouncementSingleDTO> mItems;
    private View v;
    final Context mMainContext;

    public AnnouncementItemAdapter(List<AnnouncementSingleDTO> items, Context context) {
        super();
        mItems = new ArrayList<>(items);
        mMainContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_announcement, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        AnnouncementSingleDTO item = mItems.get(i);
        viewHolder.title.setText(item.title);
        viewHolder.date.setText(item.date);

        //TODO: Hardcoded colors
        if(!item.attachment.equals(""))
            viewHolder.view.setBackgroundColor(Color.parseColor("#E71D36"));
        else
            viewHolder.view.setBackgroundColor(Color.parseColor("#B6B6B6"));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title;
        public TextView date;
        public View view;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.item_name);
            date = (TextView) itemView.findViewById(R.id.item_date);
            view = itemView.findViewById(R.id.new_announcement);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.i("asdf", String.valueOf(title.getText()));
        }
    }

    public void animateTo(List<AnnouncementSingleDTO> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<AnnouncementSingleDTO> newModels) {
        for (int i = mItems.size() - 1; i >= 0; i--) {
            final AnnouncementSingleDTO model = mItems.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<AnnouncementSingleDTO> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final AnnouncementSingleDTO model = newModels.get(i);
            if (!mItems.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<AnnouncementSingleDTO> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final AnnouncementSingleDTO model = newModels.get(toPosition);
            final int fromPosition = mItems.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public AnnouncementSingleDTO removeItem(int position) {
        final AnnouncementSingleDTO model = mItems.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, AnnouncementSingleDTO model) {
        mItems.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final AnnouncementSingleDTO model = mItems.remove(fromPosition);
        mItems.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }
}
