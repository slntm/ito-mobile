package tr.org.ito.app.activities.announcement;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import tr.org.ito.app.R;
import tr.org.ito.app.dtos.AnnouncementSingleDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ccavusoglu on 03.02.2016.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class AnnouncementFragment extends android.support.v4.app.Fragment {
    public String mLastSearchText;
    public AnnouncementItemAdapter mAdapter;

    private final List<AnnouncementSingleDTO> mItems;
    private RecyclerView mRecyclerView;

    public AnnouncementFragment(List<AnnouncementSingleDTO> itemList) {
        mItems = itemList;
    }

    public AnnouncementFragment() {
        mItems = null;
    }

    public static AnnouncementFragment newInstance(List<AnnouncementSingleDTO> itemList) {
        AnnouncementFragment myFragment = new AnnouncementFragment(itemList);
        return myFragment;
    }

    protected void filter(String query) {
        query = query.toLowerCase();

        final List<AnnouncementSingleDTO> filteredModelList = new ArrayList<>();

        for (AnnouncementSingleDTO model : mItems) {
            final String text = model.title.toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }

        mAdapter.animateTo(filteredModelList);
        mRecyclerView.scrollToPosition(0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.viewpager_announcement, null);

        mRecyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new AnnouncementItemAdapter(mItems, getContext());

        mRecyclerView.setAdapter(mAdapter);

        setRetainInstance(true);
        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
