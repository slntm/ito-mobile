package tr.org.ito.app.dtos;

/**
 * Created by Q on 01.02.2016.
 */
public class HeadlinesDTO {
    public int image;
    public String headline;
    public String partialText;
    public String fullText;

    public HeadlinesDTO(int image, String headline, String fullText) {
        this.image = image;
        this.headline = headline;
        this.partialText = fullText.substring(0, 128) + "...";
        this.fullText = fullText;
    }
}
