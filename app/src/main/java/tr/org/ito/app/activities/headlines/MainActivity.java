package tr.org.ito.app.activities.headlines;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import tr.org.ito.app.R;
import tr.org.ito.app.activities.BaseActivity;

public class MainActivity extends BaseActivity {
    private static final String SELECTED_ITEM_ID = "selected_item_id";

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    public HeadlinesItemAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new HeadlinesItemAdapter(this, getIntent().getStringExtra("search") != null);
        mRecyclerView.setAdapter(mAdapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getIntent() != null &&
                        getIntent().getStringExtra("search") != null &&
                        !getIntent().getStringExtra("search").equals("")) {
                    onCustomQuery(String.valueOf(getIntent().getStringExtra("search")));
                }
            }
        }, 150);
    }

    public boolean onCustomQuery(String newText) {
        mAdapter.filter(newText);
        mRecyclerView.scrollToPosition(0);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
