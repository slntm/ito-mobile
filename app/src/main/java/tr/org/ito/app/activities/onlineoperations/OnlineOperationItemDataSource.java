package tr.org.ito.app.activities.onlineoperations;

import tr.org.ito.app.dtos.OnlineOperationsDTO;
import tr.org.ito.app.helpers.APIManager;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by scokceken on 31.01.2016.
 */
public class OnlineOperationItemDataSource {
    private List<OnlineOperationItem> mOnlineOperationItemList;

    public OnlineOperationItemDataSource() {
        mOnlineOperationItemList = new LinkedList<OnlineOperationItem>();

        populate();
    }

    public void populate() {
        for (OnlineOperationsDTO item: APIManager.getOnlineOperationsList()) {
            mOnlineOperationItemList.add(new OnlineOperationItem(item.title, item.description, item.launchClass, item.isFavorite));
        }
    }

    public List<OnlineOperationItem> getOnlineOperationItemList() {
        return mOnlineOperationItemList;
    }
}
