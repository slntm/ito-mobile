package tr.org.ito.app.activities.onlineoperations;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import tr.org.ito.app.R;

/**
 * Created by scokceken on 02.02.2016.
 */
public class OnlineInquiryItemViewHolder extends RecyclerView.ViewHolder {
    CardView cv;
    TextView itemName;
    TextView itemDescription;
    private final Context mContext;
    boolean isFavorite;
    Class launchClass;
    OnlineOperationItem mSelfItemReference;
    
    Snackbar snackbar;
 
    OnlineInquiryItemViewHolder(View itemView, final DrawerLayout layout) {
        super(itemView);
        cv = (CardView)itemView.findViewById(R.id.cv);
        itemName = (TextView)itemView.findViewById(R.id.item_name);
        itemDescription = (TextView)itemView.findViewById(R.id.item_description);
        mContext = itemView.getContext();

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, launchClass));
            }
        });
    }
}
