package tr.org.ito.app.activities.headlines;

import android.graphics.Bitmap;

/**
 * Created by Q on 01.02.2016.
 */
public class HeadlinesItem {
    public Bitmap  image;
    public String headline;
    public String partialText;
    public String fullText;

    public HeadlinesItem(Bitmap  image, String headline, String partialText, String fullText) {
        this.fullText = fullText;
        this.headline = headline;
        this.image = image;
        this.partialText = partialText;
    }
}
