package tr.org.ito.app.activities.headlines;

import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;
import tr.org.ito.app.R;
import tr.org.ito.app.activities.BaseActivity;

/**
 * Created by Q on 02.02.2016.
 */
public class HeadlineSingleActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_headline);
        init();

        TextView title = (TextView) findViewById(R.id.headlineTitle);
        title.setText(getIntent().getStringExtra("title"));

        TextView fullText = (TextView) findViewById(R.id.headlineFullText);
        fullText.setText(getIntent().getStringExtra("fullText"));

        ImageView imageView = (ImageView) findViewById(R.id.image);
        imageView.setImageResource(getIntent().getIntExtra("image", 1));
    }
}
