package tr.org.ito.app.activities.onlineoperations;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import tr.org.ito.app.R;
import tr.org.ito.app.activities.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class OnlineOperationsActivity extends BaseActivity {
    public String mLastSearchText;
    private RecyclerView mRecyclerView;
    private OnlineOperationItemAdapter mAdapter;
    private List<OnlineOperationItem> mModels;
    private boolean mIsFavoriteOnly = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLastSearchText = "";
        setContentView(R.layout.activity_online_operations);
        init();

        DrawerLayout layout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        OnlineOperationItemDataSource mOnlineInquiryItemDataSoruce = new OnlineOperationItemDataSource();

        mModels = mOnlineInquiryItemDataSoruce.getOnlineOperationItemList();
        mAdapter = new OnlineOperationItemAdapter(mModels, layout);
        mRecyclerView.setAdapter(mAdapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getIntent() != null &&
                        getIntent().getStringExtra("search") != null &&
                        !getIntent().getStringExtra("search").equals("")) {
                    onQueryTextChange(String.valueOf(getIntent().getStringExtra("search")));
                }
            }
        }, 150);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mLastSearchText = newText;
        final List<OnlineOperationItem> filteredModelList = filter(mModels, newText);
        mAdapter.animateTo(filteredModelList);
        mRecyclerView.scrollToPosition(0);
        return true;
    }

    private List<OnlineOperationItem> filter(List<OnlineOperationItem> models, String query) {
        query = query.toLowerCase();

        final List<OnlineOperationItem> filteredModelList = new ArrayList<>();
        for (OnlineOperationItem model : models) {
            final String text = model.getName().toLowerCase();
            if (((mIsFavoriteOnly && model.isIsFavorite()) || (!mIsFavoriteOnly)) && text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
}
