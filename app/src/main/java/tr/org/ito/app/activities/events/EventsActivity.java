package tr.org.ito.app.activities.events;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import tr.org.ito.app.R;
import tr.org.ito.app.activities.BaseActivity;
import tr.org.ito.app.activities.announcement.AnnouncementFragment;
import tr.org.ito.app.activities.headlines.HeadlinesItemAdapter;
import tr.org.ito.app.activities.onlineoperations.OnlineOperationItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Q on 02.02.2016.
 */
public class EventsActivity extends BaseActivity {
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    EventItemAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        init();

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new EventItemAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getIntent() != null &&
                        getIntent().getStringExtra("search") != null &&
                        !getIntent().getStringExtra("search").equals("")) {
                    onQueryTextChange(String.valueOf(getIntent().getStringExtra("search")));
                }
            }
        }, 150);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mAdapter.filter(newText);
        mRecyclerView.scrollToPosition(0);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }
}
