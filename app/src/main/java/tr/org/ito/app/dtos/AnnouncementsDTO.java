package tr.org.ito.app.dtos;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Q on 06.02.2016.
 */
public class AnnouncementsDTO {
    public LinkedHashMap<AnnouncementDate, ArrayList<AnnouncementSingleDTO>> announcements;

    public AnnouncementsDTO(ArrayList<AnnouncementSingleDTO> announcements) {
        this.announcements = new LinkedHashMap<AnnouncementDate, ArrayList<AnnouncementSingleDTO>>();

        AnnouncementDate currentDate = new AnnouncementDate(announcements.get(0).month, announcements.get(0).year);

        ArrayList<AnnouncementSingleDTO> tempList = new ArrayList<AnnouncementSingleDTO>();
        int count = announcements.size();

        for (AnnouncementSingleDTO item : announcements) {
            boolean sameTab = false;
            count--;

            if (currentDate.year.equals(item.year)) {
                if (currentDate.month.equals(item.month)) {
                    sameTab = true;
                }
            }

            if (sameTab) {
                tempList.add(item);
            } else {
                this.announcements.put(new AnnouncementDate(currentDate.month, currentDate.year), (ArrayList<AnnouncementSingleDTO>) tempList.clone());
                currentDate = new AnnouncementDate(item.month, item.year);
                tempList = new ArrayList<AnnouncementSingleDTO>();
                tempList.add(item);

                if (count == 0) //son item eklemek icin kondu. gece 3 shittyprogramming mazur gorulsun :/
                    this.announcements.put(new AnnouncementDate(item.month, item.year), (ArrayList<AnnouncementSingleDTO>) tempList.clone());
            }
        }
    }

    public class AnnouncementDate {
        public String month;
        public String year;

        public AnnouncementDate(String month, String year) {
            this.month = month;
            this.year = year;
        }
    }
}
