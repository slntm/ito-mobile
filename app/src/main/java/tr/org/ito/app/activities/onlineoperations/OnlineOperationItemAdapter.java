package tr.org.ito.app.activities.onlineoperations;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import tr.org.ito.app.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by scokceken on 31.01.2016.
 */
public class OnlineOperationItemAdapter extends RecyclerView.Adapter<OnlineInquiryItemViewHolder>{
    private List<OnlineOperationItem> mModels;
    DrawerLayout mDrawerLayout ;

    public OnlineOperationItemAdapter(List<OnlineOperationItem> onlineOperationItemList,
        DrawerLayout layout) {
        this.mModels = new ArrayList<>(onlineOperationItemList);
        this.mDrawerLayout = layout;
    }

    @Override
    public OnlineInquiryItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_online_operations, parent, false);
        OnlineInquiryItemViewHolder onlineInquiryItemViewHolder = new OnlineInquiryItemViewHolder(view, mDrawerLayout);
        return onlineInquiryItemViewHolder;
    }

    @Override
    public void onBindViewHolder(OnlineInquiryItemViewHolder holder, int position) {
        holder.itemName.setText(mModels.get(position).getName());
        holder.itemDescription.setText(mModels.get(position).getDescription());
        holder.launchClass = mModels.get(position).getLaunchClass();
        holder.isFavorite = mModels.get(position).isIsFavorite();
        holder.mSelfItemReference = mModels.get(position);
    }

    @Override
    public int getItemCount() {
        return mModels.size();
    }

    public void animateTo(List<OnlineOperationItem> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<OnlineOperationItem> newModels) {
        for (int i = mModels.size() - 1; i >= 0; i--) {
            final OnlineOperationItem model = mModels.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<OnlineOperationItem> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final OnlineOperationItem model = newModels.get(i);
            if (!mModels.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<OnlineOperationItem> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final OnlineOperationItem model = newModels.get(toPosition);
            final int fromPosition = mModels.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public OnlineOperationItem removeItem(int position) {
        final OnlineOperationItem model = mModels.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, OnlineOperationItem model) {
        mModels.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final OnlineOperationItem model = mModels.remove(fromPosition);
        mModels.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }
}
