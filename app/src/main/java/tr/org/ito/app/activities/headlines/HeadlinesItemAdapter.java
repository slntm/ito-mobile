package tr.org.ito.app.activities.headlines;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import tr.org.ito.app.R;
import tr.org.ito.app.activities.BaseActivity;
import tr.org.ito.app.dtos.HeadlinesDTO;
import tr.org.ito.app.helpers.APIManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Q on 01.02.2016.
 */
public class HeadlinesItemAdapter extends RecyclerView.Adapter<HeadlinesItemAdapter.ViewHolder> {
    ArrayList<HeadlinesItem> mItems;
    ArrayList<HeadlinesItem> mModels;
    int mPosition = 0;
    final Context mMainContext;
    boolean redirect = false;

    public HeadlinesItemAdapter(Context context, boolean redirect) {
        super();
        mMainContext = context;
        mItems = new ArrayList<>();
        mModels = new ArrayList<>();

        this.redirect = redirect;
        populate();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (mPosition++ == 0) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.cardview_headlines, viewGroup, false);
            return new ViewHolder(v);
        } else {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.cardview_headlines_left, viewGroup, false);
            return new ViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        HeadlinesItem item = mItems.get(i);
        viewHolder.headline.setText(item.headline);
        viewHolder.partialText.setText(item.fullText);
        viewHolder.image.setImageBitmap(item.image);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void populate() {
        //if (redirect) {
            for (HeadlinesDTO item : APIManager.getHeadlinesList()) {
                mItems.add(
                        new HeadlinesItem(BitmapFactory.decodeResource(mMainContext.getResources(), item.image),
                                item.headline, item.partialText, item.fullText));
                mModels.add(
                        new HeadlinesItem(BitmapFactory.decodeResource(mMainContext.getResources(), item.image),
                                item.headline, item.partialText, item.fullText));

            }
        //} else {
//            new AsyncTask<Integer, Integer, String>() {
//
//                @Override
//                protected String doInBackground(Integer... params) {
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
//                    for (HeadlinesDTO item : APIManager.getHeadlinesList()) {
//                        mItems.add(
//                                new HeadlinesItem(BitmapFactory.decodeResource(mMainContext.getResources(), item.image),
//                                        item.headline, item.partialText, item.fullText));
//                        mModels.add(
//                                new HeadlinesItem(BitmapFactory.decodeResource(mMainContext.getResources(), item.image),
//                                        item.headline, item.partialText, item.fullText));
//                    }
//
//                    return null;
//                }
//
//                @Override
//                protected void onPostExecute(String s) {
//
//                }
//            }.execute();
//        }
    }

    public void filter(String query) {
        query = query.toLowerCase();

        final List<HeadlinesItem> filteredModelList = new ArrayList<>();

        for (HeadlinesItem model : mModels) {
            final String text = model.headline.toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }

        animateTo(filteredModelList);
    }

    public void animateTo(List<HeadlinesItem> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<HeadlinesItem> newModels) {
        for (int i = mItems.size() - 1; i >= 0; i--) {
            final HeadlinesItem model = mItems.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<HeadlinesItem> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final HeadlinesItem model = newModels.get(i);
            if (!mItems.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<HeadlinesItem> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final HeadlinesItem model = newModels.get(toPosition);
            final int fromPosition = mItems.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public HeadlinesItem removeItem(int position) {
        final HeadlinesItem model = mItems.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, HeadlinesItem model) {
        mItems.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final HeadlinesItem model = mItems.remove(fromPosition);
        mItems.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final Context mContext;
        public ImageView image;
        public TextView headline;
        public TextView partialText;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.cv_image);
            headline = (TextView) itemView.findViewById(R.id.cv_headline);
            partialText = (TextView) itemView.findViewById(R.id.cv_partial);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final Intent intent = new Intent(mContext, HeadlineSingleActivity.class);
            intent.putExtra("fullText", mItems.get(getLayoutPosition()).fullText);
            intent.putExtra("title", mItems.get(getLayoutPosition()).headline);

            switch (getLayoutPosition()) {
                case 0:
                    intent.putExtra("image", R.drawable.headline1);
                    break;
                case 1:
                    intent.putExtra("image", R.drawable.headline2);
                    break;
                case 2:
                    intent.putExtra("image", R.drawable.headline3);
                    break;
                case 3:
                    intent.putExtra("image", R.drawable.headline4);
                    break;
                case 4:
                    intent.putExtra("image", R.drawable.headline5);
                    break;
                case 5:
                    intent.putExtra("image", R.drawable.headline6);
                    break;
                case 6:
                    intent.putExtra("image", R.drawable.headline7);
                    break;
                case 7:
                    intent.putExtra("image", R.drawable.headline8);
                    break;
            }

            mContext.startActivity(intent);
        }
    }
}
