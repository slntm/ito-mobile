package tr.org.ito.app.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by Q on 06.02.2016.
 */
public class SearchHelper {
    public static LinkedHashMap<String, ArrayList<SearchResultItem>> getSearchResults(String queryText, String scope) {
        final LinkedHashMap<String, ArrayList<SearchResultItem>> resultItemList = new LinkedHashMap<>();

        queryText = queryText.toLowerCase();

        for (SearchResultItem item : APIManager.searchableItems) {
            if(!scope.equals("Haberler") && !item.category.equals(scope))
                continue;

            final String text = item.title.toLowerCase();
            if (text.contains(queryText)) {
                if (resultItemList.get(item.category) == null) {
                    resultItemList.put(item.category, new ArrayList<SearchResultItem>());
                }

                resultItemList.get(item.category).add(item);
            }
        }

        return resultItemList;
    }
}
