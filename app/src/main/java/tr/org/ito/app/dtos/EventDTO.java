package tr.org.ito.app.dtos;

/**
 * Created by Q on 07.02.2016.
 */
public class EventDTO {
    public int image;
    public String headline;
    public String date;
    public String location;

    public EventDTO(int image, String date, String headline, String location) {
        this.date = date;
        this.headline = headline;
        this.image = image;
        this.location = location;
    }
}
