package tr.org.ito.app.activities.events;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import tr.org.ito.app.R;
import tr.org.ito.app.activities.BaseActivity;
import tr.org.ito.app.activities.onlineoperations.OnlineOperationItem;
import tr.org.ito.app.dtos.EventDTO;
import tr.org.ito.app.helpers.APIManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Q on 07.02.2016.
 */
public class EventItemAdapter extends RecyclerView.Adapter<EventItemAdapter.ViewHolder> {
    ArrayList<EventItem> mItems;
    ArrayList<EventItem> mModels;

    int mPosition = 0;
    final Context mMainContext;

    public EventItemAdapter(Context context) {
        super();
        mMainContext = context;
        mItems = new ArrayList<>();
        mModels = new ArrayList<>();

        populate();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_events, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        EventItem item = mItems.get(i);
        viewHolder.headline.setText(item.headline);
        viewHolder.date.setText(item.date);
        viewHolder.location.setText(item.location);
        viewHolder.image.setImageBitmap(item.image);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void populate() {
//        new AsyncTask<Integer, Integer, String>() {
//
//            @Override
//            protected String doInBackground(Integer... params) {
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }

                for (EventDTO item : APIManager.getEvents()) {
                    mItems.add(
                            new EventItem(BitmapFactory.decodeResource(mMainContext.getResources(), item.image),
                                    item.date, item.headline, item.location));
                    mModels.add(
                            new EventItem(BitmapFactory.decodeResource(mMainContext.getResources(), item.image),
                                    item.date, item.headline, item.location));
                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String s) {
//                notifyDataSetChanged();
//            }
//        }.execute();
    }

    protected void filter(String query) {
        query = query.toLowerCase();

        final List<EventItem> filteredModelList = new ArrayList<>();

        for (EventItem model : mModels) {
            final String text = model.headline.toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }

        animateTo(filteredModelList);
    }

    public void animateTo(List<EventItem> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<EventItem> newModels) {
        for (int i = mItems.size() - 1; i >= 0; i--) {
            final EventItem model = mItems.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<EventItem> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final EventItem model = newModels.get(i);
            if (!mItems.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<EventItem> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final EventItem model = newModels.get(toPosition);
            final int fromPosition = mItems.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public EventItem removeItem(int position) {
        final EventItem model = mItems.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, EventItem model) {
        mItems.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final EventItem model = mItems.remove(fromPosition);
        mItems.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final Context mContext;
        public ImageView image;
        public TextView headline;
        public TextView date;
        public TextView location;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.cv_image);
            headline = (TextView) itemView.findViewById(R.id.cv_headline);
            date = (TextView) itemView.findViewById(R.id.item_date);
            location = (TextView) itemView.findViewById(R.id.item_location);
            mContext = itemView.getContext();
        }
    }
}