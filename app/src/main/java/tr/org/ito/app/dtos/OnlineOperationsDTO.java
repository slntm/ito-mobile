package tr.org.ito.app.dtos;

/**
 * Created by Q on 31.01.2016.
 */
public class OnlineOperationsDTO {
    public String title;
    public String description;
    public Class launchClass;
    public boolean isFavorite;

    public OnlineOperationsDTO(String title, String description, Class launchClass, boolean isFavorite) {
        this.title = title;
        this.description = description;
        this.launchClass = launchClass;
        this.isFavorite = isFavorite;
    }
}
