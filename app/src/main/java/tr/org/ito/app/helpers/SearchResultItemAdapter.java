package tr.org.ito.app.helpers;

import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import tr.org.ito.app.R;
import tr.org.ito.app.activities.announcement.AnnouncementsActivity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Q on 06.02.2016.
 */
public class SearchResultItemAdapter extends BaseAdapter {
    private final ArrayList<List<SearchResultItem>> itemList;
    private Context mContext;
    private LinkedHashMap<String, ArrayList<SearchResultItem>> list;

    public SearchResultItemAdapter(Context mContext, LinkedHashMap<String, ArrayList<SearchResultItem>> list) {
        // TODO Auto-generated constructor stub
        this.mContext = mContext;
        this.list = list;
        itemList = new ArrayList<List<SearchResultItem>>(list.values());
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.listview_search_item, parent, false);
        }

        final TextView titleText = (TextView) convertView.findViewById(R.id.search_title);
        TextView textCount = (TextView) convertView.findViewById(R.id.search_item_count);

        titleText.setTextSize(22);
        textCount.setTextSize(14);

        textCount.setGravity(Gravity.RIGHT);
        textCount.setPadding(0, 32, 32, 0);
        String key = (String) list.keySet().toArray()[position];

        titleText.setText(key);
        textCount.setText(list.get(key).size() + " Sonuç...");

        return convertView;
    }
}