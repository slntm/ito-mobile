package tr.org.ito.app.activities.announcement;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import tr.org.ito.app.activities.onlineoperations.OnlineOperationItem;
import tr.org.ito.app.dtos.AnnouncementSingleDTO;
import tr.org.ito.app.dtos.AnnouncementsDTO;
import tr.org.ito.app.helpers.APIManager;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by ccavusoglu on 03.02.2016.
 */
public class AnnouncementFragmentAdapter extends FragmentStatePagerAdapter {
    private final LinkedHashMap<AnnouncementsDTO.AnnouncementDate, ArrayList<AnnouncementSingleDTO>> mItems;

    public AnnouncementFragmentAdapter(FragmentManager fm) {
        super(fm);

        mItems = APIManager.getAnnouncementsList();
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0) {
            ArrayList<AnnouncementSingleDTO> list = new ArrayList<AnnouncementSingleDTO>();

            for (ArrayList<AnnouncementSingleDTO> item : mItems.values()) {
                list.addAll(item);
            }

            return AnnouncementFragment.newInstance(list);
        }

        return AnnouncementFragment.newInstance(new ArrayList<List<AnnouncementSingleDTO>>(mItems.values()).get(position - 1));
    }

    @Override
    public int getCount() {
        return mItems.size() + 1; //+1 Tumu icin
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0) {
            return "Tümü";
        }

            return ((AnnouncementsDTO.AnnouncementDate) mItems.keySet().toArray()[position - 1]).month
                + " "
                + ((AnnouncementsDTO.AnnouncementDate) mItems.keySet().toArray()[position - 1]).year;
    }
}
