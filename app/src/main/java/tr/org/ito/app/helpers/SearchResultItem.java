package tr.org.ito.app.helpers;

import tr.org.ito.app.activities.announcement.AnnouncementsActivity;
import tr.org.ito.app.activities.events.EventsActivity;
import tr.org.ito.app.activities.headlines.MainActivity;
import tr.org.ito.app.activities.onlineoperations.OnlineOperationsActivity;

/**
 * Created by Q on 06.02.2016.
 */
public class SearchResultItem {
    public String title;
    public String category;
    public Class launchClass;

    public SearchResultItem(String title, String category) {
        this.title = title;
        this.category = category;

        if(category.equals("Haberler"))
            launchClass = MainActivity.class;
        if(category.equals("Duyurular"))
            launchClass = AnnouncementsActivity.class;
        if(category.equals("Fuarlar"))
            launchClass = EventsActivity.class;
        if(category.equals("Online İşlemler"))
            launchClass = OnlineOperationsActivity.class;
    }
}
